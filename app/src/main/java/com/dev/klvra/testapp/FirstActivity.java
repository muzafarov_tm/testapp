package com.dev.klvra.testapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.dev.klvra.testapp.database.DBAccess;

import java.util.ArrayList;


public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        DBAccess dbAccess = DBAccess.getInstance(this);
        dbAccess.open();
        ArrayList<TestObject> list = dbAccess.getInfo();
        dbAccess.close();
        Log.d("MyTag", list.get(0).getName());
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, list);
        recyclerView.setAdapter(adapter);
    }

    public void nameClick(String name){
        Intent intent = new Intent(this, TestActivity.class);
        intent.putExtra("name", name);
        startActivity(intent);
    }
}
