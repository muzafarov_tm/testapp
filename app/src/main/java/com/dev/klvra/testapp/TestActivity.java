package com.dev.klvra.testapp;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dev.klvra.testapp.database.DBAccess;

public class TestActivity extends AppCompatActivity {

    DBAccess dbAccess;
    TestObject testObject;
    TextView nameText, sexText, phoneText, emailText, birthdateText;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Log.d("MyTag","onCreate");
        name = getIntent().getExtras().getString("name");

        dbAccess = DBAccess.getInstance(this);
        dbAccess.open();
        testObject = dbAccess.getMan(name);
        dbAccess.close();

        nameText = (TextView) findViewById(R.id.nameText);
        sexText = (TextView) findViewById(R.id.sexText);
        phoneText = (TextView) findViewById(R.id.phoneText);
        emailText = (TextView) findViewById(R.id.emailText);
        birthdateText = (TextView) findViewById(R.id.birthdateText);

        nameText.setText(testObject.getName());
        sexText.setText(testObject.getSex());
        phoneText.setText(testObject.getPhone());
        birthdateText.setText(testObject.getBirthdate());
        emailText.setText(testObject.getEmail());

        nameText.setOnClickListener(clickListener);
        sexText.setOnClickListener(clickListener);
        phoneText.setOnClickListener(clickListener);
        birthdateText.setOnClickListener(clickListener);
        emailText.setOnClickListener(clickListener);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.nameText:
                    showChangeDialog("name", nameText.getText().toString());
                    break;
                case R.id.sexText:
                    showChangeDialog("sex", sexText.getText().toString());
                    break;
                case R.id.birthdateText:
                    showChangeDialog("birthdate", birthdateText.getText().toString());
                    break;
                case R.id.phoneText:
                    showChangeDialog("phone", phoneText.getText().toString());
                    break;
                case R.id.emailText:
                    showChangeDialog("email", emailText.getText().toString());
                    break;
            }
        }
    };

    public void showChangeDialog(final String item, final String text){         //dialog for changing item
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_change, null);
        dialogBuilder.setView(dialogView);

        final EditText edit = (EditText) dialogView.findViewById(R.id.edit);
        edit.setText(text);
        edit.setSelection(text.length());
        dialogBuilder.setTitle("Change item");
        dialogBuilder.setMessage("Enter text below:");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Log.d("MyTag","Click");
                final String newText = edit.getText().toString();
                dbAccess.open();
                TestObject object = dbAccess.getMan(name);
                if(item.equals("name")) {
                    object.setName(newText);
                    Log.d("MyTag","equals " + newText);
                }
                if(item.equals("sex")) object.setSex(newText);
                if(item.equals("birthdate")) object.setBirthdate(newText);
                if(item.equals("phone")) object.setPhone(newText);
                if(item.equals("email")) object.setEmail(newText);
                Log.d("MyTag", "obj name " +object.getName());
                dbAccess.changeItem(object);
                dbAccess.close();
                refreshItems();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void refreshItems(){
        dbAccess.open();
        testObject = dbAccess.getMan(name);
        dbAccess.close();
        nameText.setText(testObject.getName());
        sexText.setText(testObject.getSex());
        phoneText.setText(testObject.getPhone());
        birthdateText.setText(testObject.getBirthdate());
        emailText.setText(testObject.getEmail());
        Log.d("MyTag", "refresh items");
    }



}
