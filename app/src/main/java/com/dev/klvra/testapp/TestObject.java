package com.dev.klvra.testapp;


public class TestObject {           //model of database item

    private String name;
    private String sex;
    private String birthdate;
    private String email;
    private String phone;

    public TestObject(String name, String sex, String birthdate, String email, String phone) {
        this.name = name;
        this.sex = sex;
        this.birthdate = birthdate;
        this.email = email;
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
