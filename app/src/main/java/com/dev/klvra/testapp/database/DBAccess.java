package com.dev.klvra.testapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dev.klvra.testapp.TestObject;


import java.util.ArrayList;

///Database wrapper


public class DBAccess {

    private final String TABLE = "testinfo";

    private DBHelper dbHelper;
    private SQLiteDatabase database;
    private static DBAccess instance;
    private TestObject testObject;
    private TestObject idealItem = new TestObject("John Smith", "male", "29.02.1977", "johnsmith@yahoo.com", "8 800 567 87 90");
    private TestObject idealItem2 = new TestObject("Anna Karenina", "female", "13.11.1985","anya85@yandex.ru", "8 965 741 57 92");
    private TestObject idealItem3 = new TestObject("Jyu Lao", "male", "29.02.1990", "superjyu@ch.com", "8 945 433 21 76");
    private ArrayList<TestObject> list;
    private ArrayList<TestObject> list2 = new ArrayList<>();

    private DBAccess(Context context) {
        this.dbHelper = new DBHelper(context);
    }

    public static DBAccess getInstance(Context context) {           //singleton with access to db
        if (instance == null) {
            instance = new DBAccess(context);
        }
        return instance;
    }

    public void open() {this.database = dbHelper.getWritableDatabase();}

    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public ArrayList<TestObject> getInfo(){
        Cursor cursor = database.query (TABLE, null, null, null, null, null, null);
        if (cursor.getCount()==0){
            fillDB();
            getInfo();
        }
        else {
                cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                String name = cursor.getString(cursor.getColumnIndex("name"));
                String sex = cursor.getString(cursor.getColumnIndex("sex"));
                String birthdate = cursor.getString(cursor.getColumnIndex("birthdate"));
                String email = cursor.getString(cursor.getColumnIndex("email"));
                String phone = cursor.getString(cursor.getColumnIndex("phone"));
                testObject = new TestObject(name, sex, birthdate, email, phone);
                list2.add(testObject);
                cursor.moveToNext();
                Log.d("MyTag", "+1");
            }
            cursor.close();
        }
        Log.d("MyTag", "getInfo");
        return list2;
    }

    public TestObject getMan(String name){
            String name_array[] = {name};
            Cursor c = database.query(TABLE, null, "name = ?", name_array, null, null, null);
            c.moveToFirst();
                String name_ = c.getString(c.getColumnIndex("name"));
                String sex = c.getString(c.getColumnIndex("sex"));
                String birthdate = c.getString(c.getColumnIndex("birthdate"));
                String email = c.getString(c.getColumnIndex("email"));
                String phone = c.getString(c.getColumnIndex("phone"));
                TestObject testObject = new TestObject(name_, sex, birthdate, email, phone);
            c.close();
            return testObject;

        }

    public void fillDB(){              //if db is empty, fill it
        list = new ArrayList<>();
        list.add(idealItem);
        list.add(idealItem2);
        list.add(idealItem3);
        Log.d("MyTag", "DB filled");
        for(int i=0; i<list.size(); i++){
            String name = list.get(i).getName();
            String sex = list.get(i).getSex();
            String birthdate = list.get(i).getBirthdate();
            String email = list.get(i).getEmail();
            String phone = list.get(i).getPhone();
            ContentValues cv = new ContentValues();
            cv.put("name", name);
            cv.put("sex", sex);
            cv.put("birthdate", birthdate);
            cv.put("email",email);
            cv.put("phone", phone);
            database.insert(TABLE, null, cv);
        }
    }

    public void changeItem(TestObject object){
        Log.d("MyTag", " new name " + object.getName());

        String name = object.getName();
        String sex = object.getSex();
        String birthdate = object.getBirthdate();
        String email = object.getEmail();
        String phone = object.getPhone();
        ContentValues cv = new ContentValues();
        cv.put("name", name);
        cv.put("sex", sex);
        cv.put("birthdate", birthdate);
        cv.put("email",email);
        cv.put("phone", phone);
        database.update(TABLE, cv, "_id=?",new String[]{"1"});


    }


}
