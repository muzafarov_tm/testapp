package com.dev.klvra.testapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CustomViewHolder> {
    private ArrayList<TestObject> list = new ArrayList<>();
    private Context context;

    public RecyclerViewAdapter(Context context, ArrayList<TestObject> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        Log.d("MyTag", "onBind");
        TestObject item = list.get(i);
        customViewHolder.textView.setText(item.getName());
        customViewHolder.textView.setOnClickListener(clickListener);
        customViewHolder.textView2.setOnClickListener(clickListener);
        customViewHolder.textView.setTag(customViewHolder);
        customViewHolder.textView2.setTag(customViewHolder);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView textView;
        protected TextView textView2;

        public CustomViewHolder(View view) {
            super(view);
            this.textView = (TextView) view.findViewById(R.id.textView);
            this.textView2 = (TextView) view.findViewById(R.id.textView2);
        }
    }

    View.OnClickListener clickListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            CustomViewHolder holder = (CustomViewHolder) v.getTag();
            int position = holder.getAdapterPosition();
            String name = list.get(position).getName();
            ((FirstActivity) context).nameClick(name);
        }
    };

}
